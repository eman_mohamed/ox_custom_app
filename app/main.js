define('helloworld/app/main', ['io.ox/core/extensions'], function () {

	'use strict';

	var app = ox.ui.createApp({
		name: 'helloworld/app',
		title: 'Hello World App' // Can't be empty! This title appears if the user clicks on the App Launcher so it should be the same like in the manifest.json file
	});

	app.setLauncher(function () {

		console.log('hello world launcher init ...');

		var win = ox.ui.createWindow({
			name: 'helloworld/app'
		});
		app.setWindow(win);

		/*
			Acquire Token for external service to create a valid session for service authentication.
			More details:
			http://oxpedia.org/wiki/index.php?title=HTTP_API#Module_.22token.22_.28since_7.4.0.29
		*/
        console.log('session sent = '+ox.session);
		$.getJSON( '/appsuite/api/token?action=acquireToken&session=' + ox.session, function( data ) {
		  $.each( data, function( key, value ) {
			if (key == 'data') {
				$.each( value, function( dkey, dvalue ) {
					var access_token = dvalue;
					console.log('got acess_token = '+access_token);
                   console.log('ox.session = '+ox.session);

					init_window_and_show(win, access_token);
				 });
			}
		  });
		});

	});

	return { getApp: app.getInstance };

});

function init_window_and_show(win, access_token) {
	// var ox_domain = 'http://localhost/helloworld-app-service.php';
	// var iframe =   $('<iframe>', { src: ox_domain, frameborder: 0 });
	// iframe.css({
	// 	width: '100%',
	// 	height: '100%'
	// });

	// win.nodes.main.append(iframe);

	// var url_with_ox_token = ox_domain + '?ox_token='+access_token;
	// iframe.attr('src', url_with_ox_token);
	// win.nodes.main.append(iframe);
	console.log(access_token);
	// var ox_domain = 'http://localhost/example.php';
     //var url_with_ox_token = ox_domain ;
	 var content = $('<div class="content"><h2>Please, fill in the form</h2></div>');
            content.append($('<BR>'));
            content.append($('<img src="http://www.joomlaworks.net/images/demos/galleries/abstract/7.jpg"  width="128" height="128">'));
            var text_1 = $('<input />', {
              type  : 'text',
              value : 'Enter your name',
              id    : 'txt_1',
              name  : 'txt_1'
           });
            content.append(text_1);
            var call_button = $('<input />', {
              type  : 'button',
              value : 'submit',
              id    : 'call_button',
              on    : {
                 click: function(id) {
                 console.log(id);
                 console.log($('#txt_1').val());
                $.ajax({
   					 type: 'GET',
  					 url: 'example.php?name='+$('#txt_1').val(),
  					// crossDomain: true, // enable this
    				 dataType: 'json' }).done(
  					  function( data ) {
                      console.log(data);
                      alert(data[0].value);
                      window.location.assign(window.location.pathname);
                    }) ;
      
                 }
              }
            });/*
           var table= $('<div id="id1" >sdsd</div>').load(function() {
            	           

            	$.ajax({
   					 type: 'GET',
  					 url: 'list.php',
    				 dataType: 'json' }).done(
  					  function( data ) {
                      console.log(data);

                    }) ;
    		});
      
  */
    var table='</br></br><table class="table">';
    table+='<tr><td>#</td><td>name</td></tr>';
    $.ajax({url: 'list.php', success: function(result){
    	result=JSON.parse(result);
    	console.log(result);
    	
    	for(var i=0;i<result.length;i++){
    		table+='<tr><td>'+(i+1)+'</td><td>'+result[i]+'</td></tr>';
    	}
    	table+='</table>';
    	content.append(table);   
}});



            //         $.getJSON('example.php?name='+$('#txt_1').val(), 
            //         	function( data ) {
            //             alert(data[0].content);
            //             console.log('here in the response');
            //             console.log(data);
            //         });
            //      }
            //   }
            // });
       
           
    content.append(call_button);  
    //content.append(table);      
    win.nodes.main.append(content);
	win.show();

	console.log('... done');
}

